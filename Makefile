CXX=x86_64-elf-gcc
CXXFLAGS=-std=c++0x -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti -nostdlib -mcmodel=large -mno-red-zone
CXXFLAGS+=-Iinclude
LDFLAGS=-ffreestanding -O2 -nostdlib -lgcc -z max-page-size=0x1000

OBJ=src/load.o \
	src/kernel.o

run: halcyon.iso
	qemu-system-x86_64 -hda $^

clean: 
	find -name "*.o" -exec rm {} \;
	find -name "*.bin" -exec rm {} \;

.PHONY: clean run image

#######
# Build
#######

src/%.o: src/%.asm
	nasm -Isrc/ -felf64 -o $@ $^

src/%.o: src/%.cc
	$(CXX) $(CXXFLAGS) -c -o $@ $^

###########
# ISO stuff
###########

halcyon.iso: kernel.bin grub.cfg
	mkdir -p iso/boot/grub
	cp kernel.bin iso/boot
	cp grub.cfg iso/boot/grub
	grub-mkrescue -o $@ iso

iso/boot/grub/grub.cfg: grub.cfg
	cp $^ $@

kernel.bin: linker.ld src/boot.o $(OBJ)
	$(CXX) -T $< $(OBJ) -o $@ $(LDFLAGS)
