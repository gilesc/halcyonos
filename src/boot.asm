[SECTION .text]
[BITS 32] 

%include "constant.asm"

extern _edata
extern _end

extern start64
extern stack

global _start
_start:
    ; Store multiboot information
	mov esi, ebx
	mov edi, eax

    jmp start32

    ALIGN 4
    multiboot:
        dd MB_MAGIC
        dd MB_FLAGS
        dd MB_CHECKSUM
        dd multiboot
        dd _start
        dd (_edata-K_VMA)
        dd (_end-K_VMA)
        dd _start

global start32
start32:
    cli

    ; Set PAE bit
    mov eax, cr4
    bts eax, 5
    mov cr4, eax

    ; Tell cr3 the page table location
    mov eax, PML4
    mov cr3, eax

    ;; Read from EFER MSR 
    mov ecx, 0xC0000080
    rdmsr
    bts eax, 8
    bts eax, 0
    wrmsr

    lgdt [pGDT32]

    ; Make a 32-bit stack
	mov esp, (stack-K_VMA) + STACK_SIZE

    ; Activate long mode by enabling paging 
    mov eax, cr0
    bts eax, 31
    mov cr0, eax

    jmp CS_KERNEL:(start64-K_VMA)
.halt:
    hlt
    jmp .halt

ALIGN 4096
pGDT32:
	dw GDT_END - GDT_TABLE - 1
	dq GDT_TABLE - K_VMA

GDT_TABLE:
	dq 0x0000000000000000	; Null Descriptor
	dq 0x00cf9a000000ffff	; CS_KERNEL32
	dq 0x00af9a000000ffff,0	; CS_KERNEL
	dq 0x00af93000000ffff,0	; DS_KERNEL
	dq 0x00affa000000ffff,0	; CS_USER
	dq 0x00aff3000000ffff,0	; DS_USER
	dq 0,0					;
	dq 0,0					;
	dq 0,0					;
	dq 0,0					;

	dq 0,0,0				; Three TLS descriptors
	dq 0x0000f40000000000	;

GDT_END:

; Page tables

ALIGN 4096
PML4:
    dq (PDPT + 0x7)
    times 255 dq 0
    dq (PDPT + 0x7)
    times 255 dq 0

ALIGN 4096
PDPT:
    dq (PD + 0x7)
    times 511 dq 0

ALIGN 4096
PD:
    %assign i 0
    %rep 25
    dq (PT + i + 0x7)
    %assign i i+4096
    %endrep
    times (512 - 25) dq 0

ALIGN 4096
PT:
    %assign i 0
    %rep 512*25
    dq (i << 12) | 0x087
    %assign i i+1
    %endrep
