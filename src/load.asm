[BITS 64]
[SECTION .text]

%include "constant.asm"

extern kernel_main

global start64
start64:
    mov rsp, ((stack - K_VMA) + STACK_SIZE)

    push CS_KERNEL

    mov rax, K_VMA >> 32
    shl rax, 32
    or rax, HigherHalf - (K_VMA & 0xffffffff00000000)
    push rax

    ret
.halt:
    hlt
    jmp .halt

HigherHalf:
    ; set up a 64 bit virtual stack
	mov rax, K_VMA >> 32
	shl rax, 32
	or rax, stack - (K_VMA & 0xffffffff00000000)
	mov rsp, rax

	; set cpu flags
	push 0
	lss eax, [rsp]
	popf

	; set the input/output permission level to 3
	; it will allow all access

	pushf
	pop rax
	or rax, 0x3000
	push rax
	popf

	; update the multiboot struct to point to a
	; virtual address
	;add rsi, (K_VMA & 0xffffffff)

	; push the parameters (just in case)
	;push rsi
	;push rdi
    ;push 123

    call kernel_main
.halt:
    hlt
    jmp .halt

GDT:                           ; Global Descriptor Table (64-bit).
    .Null: equ $ - GDT         ; The null descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0                         ; Access.
    db 0                         ; Granularity.
    db 0                         ; Base (high).
    .Code: equ $ - GDT         ; The code descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10011000b                 ; Access.
    db 00100000b                 ; Granularity.
    db 0                         ; Base (high).
    .Data: equ $ - GDT         ; The data descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10010000b                 ; Access.
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
    .Pointer:                    ; The GDT-pointer.
    dw $ - GDT - 1             ; Limit.
    dq GDT                     ; Base.

global stack
ALIGN 4096
stack:
    %rep STACK_SIZE
    dd 0
    %endrep

;HelloWorld:
    ; Blank out the screen to a blue color.
;    mov edi, 0xB8000
;    mov rcx, 500
;    mov rax, 0x1F201F201F201F20       
;    mov rax, 0
;    rep stosq                         

    ; Display "Hello World!"
;    mov edi, 0x00b8000              

;    mov rax, 0x1F6C1F6C1F651F48    
;    mov [edi],rax

;    mov rax, 0x1F6F1F571F201F6F
;    mov [edi + 8], rax

;    mov rax, 0x1F211F641F6C1F72
;    mov [edi + 16], rax
;    ret


