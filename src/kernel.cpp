#include "terminal.hpp"
#include "multiboot.h"

struct page {
    bool present        : 1;
    bool rw             : 1;
    bool user           : 1;
    bool accessed       : 1;
    uint32_t reserved   : 7;
    uint32_t frame      : 20;
};

struct page_table {
    page entries[512];
};

struct page_directory {
    page_table* entries[512];
};

struct page_directory_pointer_table {
    page_directory* entries[512];
};

struct page_map_level_4 {
    page_directory_pointer_table* entries[512];
};

//const page_map_level_4* base = (page_map_level_4*) 0x1000; 

void panic() {
    while (true) ;;
}

extern "C" 
void kernel_main(uint32_t magic, multiboot_info* mb)
{
    Terminal t;
    t.writeln("** Halcyon OS v0.0.1 **");

    if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        t.writeln("FATAL: Bad multiboot magic number!");
        panic();
    }

    t.write("Usable RAM (MB): ");
    t.writeln(mb->mem_upper / 1028);

    while (true) ;;
}


