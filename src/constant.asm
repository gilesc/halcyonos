%define STACK_SIZE 0x4000

%define MB_ALIGN    1<<0
%define MB_MEMINFO  1<<1
%define MB_MODINFO  1<<3
%define MB_AOUT     1<<16
%define MB_FLAGS    (MB_ALIGN | MB_MEMINFO | MB_AOUT)
%define MB_MAGIC    0x1BADB002
%define MB_CHECKSUM -(MB_MAGIC + MB_FLAGS)
 
%define K_LMA       0x100000
%define K_VMA       0xFFFF800000000000
%define K_OFFSET    (K_VMA - K_LMA)

%define CS_KERNEL   0x10

%define PAGE_PRESENT    (1 << 0)
%define PAGE_WRITE      (1 << 1) 


