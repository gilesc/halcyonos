#pragma once

#include <stddef.h>
#include <stdint.h>

#include "util.hpp"
 
/* Hardware text mode color constants. */
enum vga_color
{
    COLOR_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_GREEN = 2,
    COLOR_CYAN = 3,
    COLOR_RED = 4,
    COLOR_MAGENTA = 5,
    COLOR_BROWN = 6,
    COLOR_LIGHT_GREY = 7,
    COLOR_DARK_GREY = 8,
    COLOR_LIGHT_BLUE = 9,
    COLOR_LIGHT_GREEN = 10,
    COLOR_LIGHT_CYAN = 11,
    COLOR_LIGHT_RED = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN = 14,
    COLOR_WHITE = 15,
};
 
uint8_t make_color(enum vga_color fg, enum vga_color bg)
{
    return fg | bg << 4;
}
 
uint16_t make_vgaentry(char c, uint8_t color)
{
    uint16_t c16 = c;
    uint16_t color16 = color;
    return c16 | color16 << 8;
}
 
class Terminal {
    size_t WIDTH = 80;
    size_t HEIGHT= 24;

    size_t row = 0, col = 0;
    uint8_t color;
    uint16_t* buffer;
    char conv_buffer[20];

public:
    Terminal() {
        color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
        buffer = (uint16_t*) 0xB8000;
        clear();
    }

    Terminal(void* pBuffer) {
        buffer = (uint16_t*) pBuffer;
    }

    void clear() {
        for (uint32_t i = 0 ; i < (HEIGHT * WIDTH); i++)
            putchar(' ');
        row = col = 0;
    }

    void putchar(char c) {
        if (c == '\n') {
            row++;
            col = 0;
            return;
        } 

        uint32_t index = row * WIDTH + col;
        buffer[index] = make_vgaentry(c, color);
        if (++col == WIDTH) {
            col = 0;
            if (++row == HEIGHT)
                row = 0;
        }
    }

    void write(uint32_t i) {
        itoa(i, conv_buffer);
        write(conv_buffer);
    }

    void write(uint64_t i) {
        itoa(i, conv_buffer);
        write(conv_buffer);
    }

    void write(const char* data) {
        for (size_t i = 0; i<strlen(data); i++) {
            putchar(data[i]);
        }
    }

    template <typename T>
    void writeln(T t) {
        write(t);
        putchar('\n');
    }
};
