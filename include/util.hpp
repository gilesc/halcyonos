#pragma once

#include <stdint.h>
#include <stddef.h>

/* string.h */
char* strcpy(char* dest, const char* src) {
    while (*src != '\0') {
        *dest++ = *src++;
    }
    return dest;
}

size_t strlen(const char* str) {
    size_t ret = 0;
    while ( str[ret] != 0 )
        ret++;
    return ret;
}

char* memcpy(char* dest, char* src, int count) {
    for (int i=0; i<count; i++) {
        dest[i] = src[i];
    }
    return dest;
}

char* memset(char* dest, char val, int count) {
    for (int i=0; i<count; i++)
        dest[i] = val;
    return dest;
}

/* cstdlib.h */

template <typename N>
char * itoa( N value, char * str, N base = 10)
{
    char * rc;
    char * ptr;
    char * low;
    // Check for supported base.
    if ( base < 2 || base > 36 )
    {
        *str = '\0';
        return str;
    }
    rc = ptr = str;
    // Set '-' for negative decimals.
    if ( value < 0 && base == 10 )
    {
        *ptr++ = '-';
    }
    // Remember where the numbers start.
    low = ptr;
    // The actual conversion.
    do
    {
        // Modulo is negative for negative value. This trick makes abs() unnecessary.
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while ( value );
    // Terminating the string.
    *ptr-- = '\0';
    // Invert the numbers.
    while ( low < ptr )
    {
        char tmp = *low;
        *low++ = *ptr;
        *ptr-- = tmp;
    }
    return rc;
}

/* Other stuff */

typedef uint16_t port_t;

void outb(port_t port, char value) {
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

uint8_t inb(port_t port) {
    uint8_t o;
    asm volatile("inb %1, %0" : "=a" (o) : "dN" (port));
    return o;
}

uint16_t inw(port_t port) {
    uint16_t o;
    asm volatile ("inw %1, %0" : "=a" (o) : "dN" (port));
    return o;
}
