# Long mode
- Nice all-in-one: https://github.com/tpisto/pasm/blob/master/simple.asm
- Good example with complete linker script and VMA: http://wiki.osdev.org/Creating_a_64-bit_kernel

## Pure64
- [Building a bootable image from Pure64](http://www.returninfinity.com/docs/BareMetal OS - Build and Install.html)
- [Post-boot data layout](http://www.returninfinity.com/pure64-manual.html)

# Creating images on a loopback device
- http://wiki.osdev.org/Loopback_Device
- http://www.root9.net/2012/07/creating-loopback-file-system-image.html
 
# Other kernels

## [Damocles64](http://code.google.com/p/damocles64/)
- relatively simple, 64-bit

# ELF
 
The default ELF section order and attributes:
- http://www.nasm.us/doc/nasmdoc7.html#section-7.9.2 */

# cstdlib, printf, etc.
- Lots implemented in the multiboot example kernel: http://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Boot-information-format
 
# File systems
 
## FAT16
- [Detailed layout](http://www.maverick-os.dk/FileSystemFormats/FAT16_FileSystem.html)
